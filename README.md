# HCRoots: Hyperbolic Complex Root solver

Complex root solver based on hyperbolic approximations.

## Installation

The solver depends on `NumPy` and can be installed with :

    sudo pip install git+https://gitlab.inria.fr/gmoro/hcroots.git

or after cloning this repository with :

    sudo python setup.py install

## Example of usage

By default the solver returns only the roots that have been guaranteed with
a Kantorovich criterion.

    import numpy as np
    import hcroots as hc

    np.random.seed(0)
    p = np.random.normal(size=25000)
    sols = hc.solve(p, m=35)

For smaller precision, the solutions that have not been guaranteed can
still be valid solutions.

    
    sols_approx = hc.solve(p, m=20, guarantee=False)
    
    # Compute the Haussdorf distance
    from scipy.spatial import cKDTree
    Qguarantee = cKDTree(sols.view(float).reshape(-1,2))
    Qapprox    = cKDTree(sols_approx.view(float).reshape(-1,2))
    d1, i1 = Qguarantee.query(sols_approx.view(float).reshape(-1,2))
    d2, i2 = Qapprox.query(sols.view(float).reshape(-1,2))
    max(d1.max(),d2.max())

## Algorithm and benchmark

The algorithm implemented in this solver is explained in the preprint
available [here]. 

The code used for the benchmark in the preprint is in the file [`benchmark.py`] of this
repository. It depends on the packages `mpsolve`, `scipy` and `matplotlib`.

[here]: https://hal.archives-ouvertes.fr/hal-03249123
[`benchmark.py`]: https://gitlab.inria.fr/gmoro/hcroots/-/blob/master/benchmark.py
