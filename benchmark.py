from matplotlib import pyplot as plt
import time
import ctypes
import numpy as np
import mpsolve
import hcroots
from scipy.spatial import cKDTree

def time_mpsolve(f, m):
    ctx = mpsolve.Context()
    q = mpsolve.MonomialPoly(ctx, f.size-1)
    for i in range(f.size):
        q.set_coefficient(i, f[i].item())
    ctx.set_input_poly(q)
    mpsolve._mps.mps_context_select_algorithm(ctx._c_ctx, mpsolve.Algorithm.SECULAR_GA)
    mpsolve._mps.mps_context_set_output_prec(ctx._c_ctx, ctypes.c_long(m))
    mpsolve._mps.mps_context_set_output_goal(ctx._c_ctx, mpsolve.Goal.MPS_OUTPUT_GOAL_APPROXIMATE)
    start = time.perf_counter()
    mpsolve._mps.mps_mpsolve(ctx._c_ctx)
    dt = time.perf_counter() - start
    sols = np.array(ctx.get_roots())
    return sols, dt

def time_hcroots(f, m):
    start = time.perf_counter()
    sols = hcroots.solve(f, m, guarantee=False)
    dt = time.perf_counter() - start
    return sols, dt

def distance(s1, s2):
    if(s1.size != s2.size):
        return np.inf
    Q = cKDTree(s1.view(float).reshape(-1,2))
    d, i = Q.query(s2.view(float).reshape(-1,2))
    return d.max(), np.unique(i).size

# Initialisation of seed and precision
m = 30

# Time measures
Ld, Lhct, Lmpt = [], [], []
for i in range(1, 11):
    d = 1000*i
    print('d = {0}'.format(d))
    np.random.seed(0)
    f = np.random.normal(size=d+1)
    s1, hct = time_hcroots(f, m)
    print('hct = {0}'.format(hct))
    s2, mpt = time_mpsolve(f, m)
    print('mpt = {0}'.format(mpt))
    print('--- max dist: {0}, num correct roots: {1} ---'.format(*distance(s1, s2)))
    Ld.append(d)
    Lhct.append(hct)
    Lmpt.append(mpt)

plt.rc('text', usetex=True)
plt.rc('font', size=16, family='serif')
plt.ylabel(r"Time in seconds")
plt.xlabel(r"Degree")
plt.plot(Ld, Lhct, label=r"HCRoots (m=30)", color="green")
plt.plot(Ld, Lmpt, label=r"MPSolve", color="blue")
plt.grid(True)
plt.legend()
plt.savefig("benchmark.pdf", bbox_inches='tight')
plt.show()
