#       Copyright (C) 2021 Guillaume Moroz <guillaume.moroz@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  https://www.gnu.org/licenses/

import numpy as np

# Compute the disks of a hyperbolic covering
def disks(d, m):
    N = np.math.ceil(np.log2(3*np.e*d/min(m-1,d)))
    r = 1 - 1/2**(np.arange(N+1))
    r[-1] = 1
    gamma = 1/2*(r[1:] + r[:-1])
    rho = 3/4*(r[1:] - r[:-1])
    K = np.ceil(3*np.pi*r[1:]/(np.sqrt(5)*rho)).astype(int)
    K[0] = 4
    return gamma, rho, K

# Compute the m-hyperbolic approximation
def hyperbolic_approximation(coeffs, m=30):
    d = coeffs.shape[-1]
    shape = coeffs.shape[:-1]
    gamma, rho, K = disks(d, m)
    N = gamma.size
    Kmax = ((d-1)//K.max()+1)*K.max()
    r = rho/gamma
    D = np.arange(d)
    G = np.zeros(shape + (N, Kmax, m), dtype='complex128')
    P = gamma[:, np.newaxis]**D * coeffs[..., np.newaxis, :]
    G[...,0] = np.fft.fft(P, Kmax)
    for i in range(m-1):
        P *= (D-i)/(i+1) * r[:, np.newaxis]
        G[..., i+1] = np.fft.fft(P[...,i+1:], Kmax)
    return G, gamma, rho, K

# Get the indices to match points to the corresponding disk
def get_indices(N, Kmax, points):
    module_indices = np.zeros(points.shape, int)
    angle_indices = np.zeros(points.shape, int)
    apoints = np.abs(points)
    big = apoints > 1-1/2**(N-1)
    small = apoints < 1/2
    middle = ~small & ~big
    module_indices[middle] = np.log2(1/(1-apoints[middle])).astype(int)
    module_indices[big] = N-1
    angle_indices[:] = (0.5 - np.angle(points)*(Kmax/(2*np.pi)) % Kmax).astype(int)
    return module_indices, angle_indices

# Evaluate the points in a unit disk
def eval_unitdisk(G, gamma, rho, points):
    N, Kmax, m = G.shape
    m_ind, a_ind = get_indices(N, Kmax, points)
    shift_points = (points - gamma[m_ind]*np.exp(-2j*np.pi*a_ind/Kmax))/rho[m_ind]
    res = np.polynomial.polynomial.polyval( shift_points, G[m_ind, a_ind].T, tensor=False)
    return res

# Evaluate the points in the complex plane
def eval_hyperbolic_approximation(covering, points):
    G, gamma, rho, d = covering
    points = np.array(points)
    apoints = np.abs(points)
    inpoints = points[apoints <= 1]
    outpoints = points[apoints > 1]
    res = np.zeros(points.size, dtype='complex128')
    res[apoints <= 1] = eval_unitdisk(G[0], gamma, rho, inpoints)
    res[apoints >  1] = eval_unitdisk(G[1], gamma, rho, 1/outpoints)*outpoints**d
    return res

# Compute the hyperbolic approximation used for multi-point evaluation
def get_hyperbolic_approximation(p, m=30):
    d = len(p)-1
    dtype = p.dtype if hasattr(p, 'dtype') else 'complex128'
    coeffs = np.zeros((2, d+1), dtype=dtype)
    coeffs[0] = p
    coeffs[1] = coeffs[0,::-1]
    G, gamma, rho, K = hyperbolic_approximation(coeffs, m)
    covering = G, gamma, rho, d
    return covering

# Compute the hyperbolic approximation and evaluate the points
def eval(p, points, m=30):
    covering = get_hyperbolic_approximation(p, m)
    res = eval_hyperbolic_approximation(covering, points)
    return res


