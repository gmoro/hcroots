#       Copyright (C) 2021 Guillaume Moroz <guillaume.moroz@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  https://www.gnu.org/licenses/

import numpy as np

# Compute the disks of a hyperbolic covering
def disks(d, m):
    N = np.math.ceil(np.log2(3*np.e*d/min(m-1,d)))
    r = 1 - 1/2**(np.arange(N+1))
    r[-1] = 1
    gamma = 1/2*(r[1:] + r[:-1])
    rho = 3/4*(r[1:] - r[:-1])
    K = np.ceil(3*np.pi*r[1:]/(np.sqrt(5)*rho)).astype(int)
    K[0] = 4
    return gamma, rho, K

# Compute the m-hyperbolic approximation
def hyperbolic_approximation(coeffs, m=30):
    d = coeffs.shape[-1]
    shape = coeffs.shape[:-1]
    gamma, rho, K = disks(d, m)
    N = gamma.size
    Kmax = ((d-1)//K.max()+1)*K.max()
    r = rho/gamma
    D = np.arange(d)
    G = np.zeros(shape + (N, Kmax, m), dtype='complex128')
    P = gamma[:, np.newaxis]**D * coeffs[..., np.newaxis, :]
    G[...,0] = np.fft.fft(P, Kmax)
    for i in range(m-1):
        P *= (D-i)/(i+1) * r[:, np.newaxis]
        G[..., i+1] = np.fft.fft(P[...,i+1:], Kmax)
    return G, gamma, rho, K

# Solve polynomials of small degree
def solve_small(p, m=30, guarantee=True, e=0):
    result = [np.empty(0)]*p.shape[0]
    abs_p = np.abs(p)
    nosol = abs_p[:,0] > abs_p[:,1:].sum(axis=-1)
    unksol = ~nosol
    sols = list(map(np.polynomial.polynomial.polyroots, p[unksol]))
    for i,j in enumerate(np.flatnonzero(unksol)):
        result[j] = sols[i][np.abs(sols[i])<=1]
    if guarantee:
        validate(result, p, e)
    return result

# Guarantee that there has a unique solution nearby
def validate(sols, p, e):
    nonempty = [i for i,x in enumerate(sols) if x.size>0]
    p0 = p[nonempty]
    p1 = np.polynomial.polynomial.polyder(p0, axis=-1)
    p2 = np.polynomial.polynomial.polyder(p1, axis=-1)
    s = np.linalg.norm(p2, 1, axis=-1)
    for i, j in enumerate(nonempty):
       q = 10*s[i]*(np.abs(np.polynomial.polynomial.polyval(sols[j], p0[i]))+e)/\
                   (np.abs(np.polynomial.polynomial.polyval(sols[j], p1[i]))-e)**2
       sols[j] = sols[j][q <= 1]


# Solve using truncated polynomials
def solve_piecewise(G, gamma, rho, K, m=30, rtol=8, guarantee=True, e=0):
    result = np.array([],dtype='complex128')
    Kmax = G.shape[1]
    for p, g, r, Kn in zip(G,gamma,rho,K):
        step = (Kmax-1)//(Kn-1)  # step * (Kn-1) < Kmax
        w = np.exp(-2j*np.pi*np.arange(0,Kmax,step)/Kmax)
        sols = solve_small(p[::step], m, guarantee, e)
        for i in range((Kmax-1)//step + 1):
            sols[i] = g*w[i] + r*sols[i]
            result = np.append(result, sols[i])
    rounded = np.round(result, decimals=-int(np.log10(rtol)))
    _, ind = np.unique(rounded, return_index=True)
    return result[ind]

# truncate and solve a polynomial over the complex
def solve(p, m=30, rtol=None, guarantee=True):
    rtol =  max(3*2**(-m), 2**-35) if rtol is None else rtol
    dtype = p.dtype if hasattr(p, 'dtype') else 'complex128'
    p = np.trim_zeros(p, 'b')
    coeffs = np.zeros((2, len(p)), dtype=dtype)
    coeffs[0] = p
    coeffs[1] = coeffs[0,::-1]
    G, gamma, rho, K = hyperbolic_approximation(coeffs, m)
    e = 3*np.linalg.norm(coeffs[0], 1)*(m+2)/2**m
    sols = solve_piecewise(G[0], gamma, rho, K, m, rtol, guarantee, e)
    invsols = solve_piecewise(G[1], gamma, rho, K, m, rtol, guarantee, e)
    result = np.concatenate([sols , 1/invsols])
    rounded = np.round(result, decimals=-int(np.log10(rtol)))
    _, ind = np.unique(rounded, return_index=True)
    return result[ind]

