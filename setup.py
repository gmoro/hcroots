#!/usr/bin/env python

#       Copyright (C) 2021 Guillaume Moroz <guillaume.moroz@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  https://www.gnu.org/licenses/

from distutils.core import setup

setup(name='HCRoots',
      version='0.1',
      description='Complex root solver based on hyperbolic covering',
      author='Guillaume Moroz',
      author_email='guillaume.moroz@inria.fr',
      python_requires = '>=3',
      install_requires = ['numpy'],
      url='https://gitlab.inria.fr/gmoro/hcroots',
      py_modules = ['hcroots'],
     )
